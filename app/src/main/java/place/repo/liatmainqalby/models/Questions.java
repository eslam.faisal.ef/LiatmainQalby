package place.repo.liatmainqalby.models;

import android.os.Parcelable;

import java.io.Serializable;

public class Questions implements Serializable {

    private String question;
    private String answer;

    public Questions() {
    }

    public Questions(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
