package place.repo.liatmainqalby;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import place.repo.liatmainqalby.models.Questions;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionsViewHolder> {

    private  ArrayList<Questions> questionsList ;
    private Context context;

    public QuestionsAdapter(ArrayList<Questions> questionsList, Context context) {
        this.questionsList = questionsList;
        this.context = context;
    }

    @NonNull
    @Override
    public QuestionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_item_list, parent, false);

        return new QuestionsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final QuestionsViewHolder holder, final int position) {

        holder.question.setText(questionsList.get(position).getQuestion());

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,Answer.class);
                intent.putExtra("answer",questionsList.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }

    public class QuestionsViewHolder extends RecyclerView.ViewHolder {

        View mainView;
        TextView question;

        public QuestionsViewHolder(View itemView) {
            super(itemView);
            mainView = itemView;
            question = itemView.findViewById(R.id.question_item_list);

        }
    }

    public void add(Questions question) {
        questionsList.add(question);
        notifyDataSetChanged();
    }

}
