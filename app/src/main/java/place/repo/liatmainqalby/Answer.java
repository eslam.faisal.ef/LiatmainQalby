package place.repo.liatmainqalby;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import place.repo.liatmainqalby.models.Questions;


public class Answer extends AppCompatActivity {

    TextView question ,answer ;
    Questions tmp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
        getSupportActionBar().setTitle("الإجابة");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        question = findViewById(R.id.question_answer_activity);
        answer = findViewById(R.id.answer_answer_activity);

        tmp = (Questions) getIntent().getSerializableExtra("answer");

        question.setText(tmp.getQuestion());
        answer.setText(tmp.getAnswer());
    }

}
